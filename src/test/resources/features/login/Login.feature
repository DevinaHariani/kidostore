
Feature: Login
  Scenario: Login with valid credentials
    Given I visited login page
    Then I logged in with "user23@gmail.com" and password "abcd1234"

   Scenario: UnsuccessFul Login with invalid credentials
      Given I visited login page
      Then I logged in with "user2@gmail." and password "abcd"
      Then I should see login failure message

   Scenario: UnsuccessFul Login with incorrect password
    Given I visited login page
    Then I logged in with "user23@gmail.com" and password "abcd"
    Then I should see mismatch credentials

      Scenario: Submitting without entering username and password
        Given I visited login page
        Then I submit without entering username and password
        Then I should see validation message