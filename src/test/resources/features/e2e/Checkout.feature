@Checkout
  Feature: Checkout cart

    Background: User is logged in
      Given I visited login page
      Then I logged in with "user23@gmail.com" and password "abcd1234"

    Scenario: Checkout cart
      When I opened class kit tab
      Then I opened random product from class kit tab
      And increased quantity and added product to cart
      Then Opened cart
      And Checked total shown in cart total section
      Then Checked out the cart
