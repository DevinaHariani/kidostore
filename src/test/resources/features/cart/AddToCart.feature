@AddToCart @Cart
Feature: Add to Cart
  Background: User is logged in
    Given I visited login page
    Then I logged in with "user23@gmail.com" and password "abcd1234"

    Scenario: Adding product to cart
      Given Cart is empty and i am on home page
      When I opened class kit tab
      Then I opened random product from class kit tab
      And increased quantity and added product to cart
      Then Opened cart and checked added product details