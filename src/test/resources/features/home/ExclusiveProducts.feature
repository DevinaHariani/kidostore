@Home
Feature: Exclusive Products
  Background: User is logged in
    Given I visited login page
    Then I logged in with "user23@gmail.com" and password "abcd1234"

    Scenario: Check number of products in Class Kit tab
      Given I opened class kit tab
      Then Only required number of products featured on class kit tab

  Scenario: Check number of products in Uniforms Kit tab
    Given I opened uniforms tab
    Then Only required number of products featured on uniforms tab