@Search
  Feature: Search a product

    Scenario: Login with valid credentials
      Given I visited login page
      Then I logged in with "user23@gmail.com" and password "abcd1234"

      @SearchWithExactName
      Scenario: search product with full name
        Given I searched product "Blossom Bumper Activity Books for Kids in English | Part A Book with 55 Fun Activities | 3 to 4 Year Old Children"

        @SearchWithKeyword
        Scenario Outline: Search product with outlines
          Given I searched product with keyword <keyword>
          Examples:
          | keyword |
          | "blossom"   |
          | "colouring" |
          | "writing"   |