package tests.cart;

import org.openqa.selenium.WebDriver;
import validations.cart.CartValidation;
import validations.home.exclusiveProducts.ExclusiveProductValidation;
import validations.loader.LoaderValidation;
import validations.navigation.NavigationValidation;
import validations.singleProduct.SingleProductValidation;

import java.util.HashMap;

public class AddToCartTests {

    WebDriver driver;
    ExclusiveProductValidation exclusiveProductValidation;
    LoaderValidation loader;
    SingleProductValidation singleProduct;
    NavigationValidation navigation;
    HashMap<String, Object> productData;
    CartValidation cart;

    public AddToCartTests(WebDriver driver) {

        exclusiveProductValidation = new ExclusiveProductValidation(driver);
        loader = new LoaderValidation(driver);
        singleProduct = new SingleProductValidation(driver);
        navigation = new NavigationValidation(driver);
        cart = new CartValidation(driver);

    }

    public void openRandomProductOfClassKitTab() throws InterruptedException {
        exclusiveProductValidation.openClassKitSection();
    }


    public void increaseQuantityAndAddProductTests() {
        loader.waitForLoaderToGetInvisible();
        singleProduct.increaseQuantity();
        singleProduct.clickOnAddToCartButton();
        singleProduct.storeProductData();
        singleProduct.clickOnAddToCartButton();
        singleProduct.validateSuccessfullyAddedToCartMessage();

    }

    public void openCartAndCheckProductDetails() {
        loader.waitForLoaderToGetInvisible();
        productData = singleProduct.getProductData();
        navigation.goToCart();
        String actualProductName = (String)productData.get("title");
        loader.waitForLoaderToGetInvisible();
        cart.validateProductName(actualProductName);
        cart.validateProductPrice(actualProductName, (float) productData.get("price"));
        cart.validateProductQuantity(actualProductName, (int) productData.get("quantity"));
        cart.validateTotal(actualProductName, (int) productData.get("quantity"), (float) productData.get("price"));

    }

    public void emptyCartAndGoToHome() throws InterruptedException {
        if (navigation.getCartItemCount() > 0) {
            loader.waitForLoaderToGetInvisible();
            navigation.goToCart();
            loader.waitForLoaderToGetInvisible();
            cart.emptyCart();
            navigation.goToHome();
        }
    }

}
