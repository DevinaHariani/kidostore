package tests.e2e;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import validations.cart.CartValidation;
import validations.checkout.CheckoutValidation;
import validations.loader.LoaderValidation;
import validations.navigation.NavigationValidation;

public class CheckoutTests {
    WebDriver driver;
    WebDriverWait wait;
    CartValidation cart;
    CheckoutValidation checkout;
    NavigationValidation navigation;
    LoaderValidation loader;

    public CheckoutTests(WebDriver driver) {
        cart = new CartValidation(driver);
        loader = new LoaderValidation(driver);
        navigation = new NavigationValidation(driver);

    }

    public void openCart(){
        navigation.goToCart();
    }
    public void validateCartTotalAmount(){
        loader.waitForLoaderToGetInvisible();
        cart.validateCartTotal();

    }
    public void checkOutCartAndCheckedProductDetails(WebDriver driver) throws InterruptedException {
        cart.storeProductsForCheckOut();
        cart.clickOnCheckOutButton(driver);
    }
}
