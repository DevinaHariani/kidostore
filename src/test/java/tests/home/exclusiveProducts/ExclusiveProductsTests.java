package tests.home.exclusiveProducts;

import io.cucumber.java.sl.In;
import org.openqa.selenium.WebDriver;
import validations.home.exclusiveProducts.ExclusiveProductValidation;
import validations.loader.LoaderValidation;

public class ExclusiveProductsTests {

    WebDriver driver;
    ExclusiveProductValidation exclusiveProductValidation;
    LoaderValidation loader;

    public ExclusiveProductsTests(WebDriver driver) {

        exclusiveProductValidation = new ExclusiveProductValidation(driver);
        loader = new LoaderValidation(driver);
    }
    public void classKitValidation() throws InterruptedException {

        exclusiveProductValidation.openClassKitSection();
    }

    public void openUniformSection() throws InterruptedException {
        exclusiveProductValidation.openUniformSection();
    }

    public void openBooksSection() throws InterruptedException {
        loader.waitForLoaderToGetInvisible();
        exclusiveProductValidation.openBooksSection();
    }

    public void  openNoteBooksSection() throws InterruptedException{
        exclusiveProductValidation.openNoteBooksSection();
    }
    public void validateNumberOfProductFeatureOnClassKitTab() throws InterruptedException{
        exclusiveProductValidation.validateNumberOfProductFeatureOnClassKitTab();
    }

    public void validateNumberOfProductFeatureOnUniformTab() throws InterruptedException{
        exclusiveProductValidation.validateNumberOfProductFeatureOnUniformTab();
    }

    public void openRandomProductFromClassKitTab() throws InterruptedException{
        exclusiveProductValidation.openRandomProductFromClassKitTab();
    }

}
