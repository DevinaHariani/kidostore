package tests.search;

import org.openqa.selenium.WebDriver;
import validations.loader.LoaderValidation;
import validations.navigation.NavigationValidation;
import validations.search.SearchModalValidations;
import validations.search.SearchResultPageValidation;

public class SearchTests {

    NavigationValidation navigation;
    SearchModalValidations searchModal;
    SearchResultPageValidation searchResultPage;
    LoaderValidation loader;

    public SearchTests(WebDriver driver) {
        navigation = new NavigationValidation(driver);
        searchModal = new SearchModalValidations(driver);
        searchResultPage = new SearchResultPageValidation(driver);
        loader = new LoaderValidation(driver);
    }

    public void searchWithExactProductNameAndValidation(String searchValue){
        loader.waitForLoaderToGetInvisible();
        navigation.clickOnSearchIcon();
        searchModal.searchProduct(searchValue);
        loader.waitForLoaderToGetInvisible();
        searchResultPage.validateProductSearchWithExactName(searchValue);

    }

    public void searchWithKeywordsAndValidate(String searchedKeywords){
        loader.waitForLoaderToGetInvisible();
        navigation.clickOnSearchIcon();
        searchModal.searchProduct(searchedKeywords);
        loader.waitForLoaderToGetInvisible();
        searchResultPage.validateProductSearchedWithKeywords(searchedKeywords);
    }

}
