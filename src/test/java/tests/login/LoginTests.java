package tests.login;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import validations.loader.LoaderValidation;
import validations.location.Location;
import validations.login.LoginValidation;

public class LoginTests {
    LoginValidation login;
    Location location;
    LoaderValidation loader;

    public LoginTests(WebDriver driver) {
        login = new LoginValidation(driver);
        location = new Location(driver);
        loader = new LoaderValidation(driver);
    }

    public void visitLoginPage(){
        location.visitLoginPage();
    }

    public void login(String username, String password){
        loader.waitForLoaderToGetInvisible();
        login.enterUsername(username);
        login.enterPassword(password);
        login.clickOnLogInButton();
    }
    //from here negative test cases
    public void verifyLoginFailure(){
        Assert.assertTrue(login.isErrorMessageDisplayed(), "Error Message is not displayed");
        Assert.assertEquals(login.getErrorMessageText(), "Please enter an e-mail address.");
    }
    public void wrongPassword(){
        Assert.assertTrue(login.isErrorMessageDisplayed(), "Error message is not displayed");
        Assert.assertEquals(login.getErrorMessageText(), "These credentials do not match our records.");
    }

    public void verifyValidations(){
        loader.waitForLoaderToGetInvisible();
        login.clickOnLogInButton();
    }
    public void verifyValidationsText(){

        Assert.assertTrue(login.isErrorMessageDisplayed(), "Error message is not displayed");
        Assert.assertEquals(login.getErrorMessageText(), "Please fill out this field.");
    }
}
