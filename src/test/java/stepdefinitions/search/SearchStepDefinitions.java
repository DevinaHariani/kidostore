package stepdefinitions.search;

import factory.DriverFactory;
import io.cucumber.java.en.Given;
import org.openqa.selenium.WebDriver;
import tests.search.SearchTests;

public class SearchStepDefinitions {
    WebDriver driver;
    SearchTests searchTests;

    public SearchStepDefinitions() {
        this.driver = DriverFactory.getDriver();
        searchTests = new SearchTests(driver);
    }

    @Given("I searched product {string}")
    public void iSearchedProduct(String searchValue) {
        searchTests.searchWithExactProductNameAndValidation(searchValue);
    }
    @Given("I searched product with keyword {string}")
    public void iSearchedProductWithKeyword(String searchedKeyword) {
        searchTests.searchWithKeywordsAndValidate(searchedKeyword);
    }
}
