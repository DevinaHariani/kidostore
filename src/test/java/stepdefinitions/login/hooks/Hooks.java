package stepdefinitions.login.hooks;

import factory.DriverFactory;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class Hooks {
    DriverFactory driverFactory = new DriverFactory();

    @Before
    public void openBrowser(){
        driverFactory.initDriver();
    }

    @After
    public void tearDown(){
        DriverFactory.getDriver().quit();
    }
}
