package stepdefinitions.login;

import factory.DriverFactory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import tests.login.LoginTests;

public class LoginStepDefinitions {
    WebDriver driver;
    LoginTests loginTests;

    public LoginStepDefinitions() {
        driver = DriverFactory.getDriver();
        loginTests = new LoginTests(driver);
    }

    @Given("I visited login page")
    public void iVisitedLoginPage() {
        loginTests.visitLoginPage();
    }

    @Then("I logged in with {string} and password {string}")
        public void iLoggedInWithUsernameAndPassword(String username, String password){
            loginTests.login(username, password);
        }
        @Then("I should see login failure message")
        public void iShouldSeeLoginFalureMessage(){
        loginTests.verifyLoginFailure();
        }
        @Then("I should see mismatch credentials")
        public void wrongPassword(){
        loginTests.wrongPassword();
        }
        @Then("I submit without entering username and password")
        public void submittingWithoutUsernameAndPassword(){
        loginTests.verifyValidations();
        }
    @Then("I should see validation message")
    public void getValidationText(){
        loginTests.verifyValidationsText();
}



    }
