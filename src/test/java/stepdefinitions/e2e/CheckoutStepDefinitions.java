package stepdefinitions.e2e;

import factory.DriverFactory;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import tests.e2e.CheckoutTests;

public class CheckoutStepDefinitions {

    WebDriver driver;
    CheckoutTests checkoutTests;

    public CheckoutStepDefinitions() {
        driver = DriverFactory.getDriver();
        checkoutTests = new CheckoutTests(driver);
    }

    @Then("Opened cart")
    public void openCart() {
        checkoutTests.openCart();
    }
    @And("Checked total shown in cart total section")
    public void checkedTotalShownInCartTotalSection() {
        checkoutTests.validateCartTotalAmount();
    }
    @Then("Checked out the cart")
    public void checkedOutTheCart() throws InterruptedException {
        checkoutTests.checkOutCartAndCheckedProductDetails(driver);
    }
}
