package stepdefinitions.cart;

import factory.DriverFactory;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import tests.cart.AddToCartTests;
import tests.home.exclusiveProducts.ExclusiveProductsTests;
import validations.loader.LoaderValidation;

public class AddToCartStepDefinitions {

    WebDriver driver;
    ExclusiveProductsTests exclusiveProductsTests;
    AddToCartTests addToCartTests;
    LoaderValidation loader;

    public AddToCartStepDefinitions() {

        driver = DriverFactory.getDriver();
        addToCartTests = new AddToCartTests(driver);
        exclusiveProductsTests = new ExclusiveProductsTests(driver);
        loader = new LoaderValidation(driver);
    }
    @Given("Cart is empty and i am on home page")
    public void cartIsEmpty() throws InterruptedException{
        addToCartTests.emptyCartAndGoToHome();
    }

    @Then("I opened random product from class kit tab")
    public void iOpenedRandomProductFromClassKitTab() throws InterruptedException {
       exclusiveProductsTests.openRandomProductFromClassKitTab();

    }

    @And("increased quantity and added product to cart")
    public void increaseQuantityAndAddedProductToCart() throws InterruptedException {
        addToCartTests.increaseQuantityAndAddProductTests();
    }
    @Then("Opened cart and checked added product details")
    public void openedCartAndCheckedAddedProductDetails(){
        addToCartTests.openCartAndCheckProductDetails();
    }

}
