package stepdefinitions.home.exclusiveProducts;

import factory.DriverFactory;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import tests.home.exclusiveProducts.ExclusiveProductsTests;
import validations.home.exclusiveProducts.ExclusiveProductValidation;
import validations.loader.LoaderValidation;

public class ExclusiveProductsStepDefinitions {

    WebDriver driver;
    ExclusiveProductsTests exclusiveProductsTests;
    LoaderValidation loader;

    public ExclusiveProductsStepDefinitions() {
        driver = DriverFactory.getDriver();
        exclusiveProductsTests = new ExclusiveProductsTests(driver);

    }



    @Given("I opened class kit tab")
        public void iOpenedClassKitTab() throws InterruptedException{
            exclusiveProductsTests.classKitValidation();

        }
        @Then("Only required number of products featured on class kit tab")
         public void onlyRequiredNumberOfProductsFeaturedOnClassKitTab() throws InterruptedException{
        exclusiveProductsTests.validateNumberOfProductFeatureOnClassKitTab();
        }

        @Given("I opened uniforms tab")
        public void iOpenedUniformsTab() throws InterruptedException{
        exclusiveProductsTests.openUniformSection();
        }

        @Then("Only required number of products featured on uniforms tab")
        public void onlyRequiredNumberOfProductsFeaturedOnUniformTab() throws InterruptedException{
        exclusiveProductsTests.validateNumberOfProductFeatureOnUniformTab();
        }






}
