package testrunner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.DataProvider;

//With JUnit
//@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/features"},
        glue = {"stepdefinitions"}
)
public class TestRunner extends AbstractTestNGCucumberTests {
@Override
    @DataProvider(parallel = true)
    public Object[][] scenarios() { return  super.scenarios(); }
}
