package validations.loader.constants;

import org.openqa.selenium.By;

public interface LoaderConstants {

    By PRELOADER = By.xpath("//div[contains(@class,'preloader')]");
}
