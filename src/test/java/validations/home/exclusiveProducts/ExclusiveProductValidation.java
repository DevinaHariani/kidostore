package validations.home.exclusiveProducts;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import utils.DriverUtils;
import validations.home.exclusiveProducts.constants.ExclusiveProductsConstants;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class ExclusiveProductValidation implements ExclusiveProductsConstants {

    WebDriver driver;
    Actions actions;
    WebDriverWait wait;

    JavascriptExecutor js;

    public  ExclusiveProductValidation(WebDriver driver){
        this.driver = driver;
        wait=new WebDriverWait(driver, Duration.ofSeconds(20));
        actions = new Actions(driver);
        js = (JavascriptExecutor) driver;
    }

    public void openClassKitSection() throws InterruptedException{
        WebElement classKitTab = driver.findElement(CLASS_KIT_TAB);
        DriverUtils.scrollElementToCenter(classKitTab, driver);
        classKitTab.click();
    }

    public void openUniformSection() throws InterruptedException{
        WebElement uniformsTab = driver.findElement(UNIFORMS_TAB);
        DriverUtils.scrollElementToCenter(uniformsTab, driver);
       uniformsTab.click();
    }

    public void openBooksSection() throws InterruptedException{
        WebElement booksTab = driver.findElement(BOOKS_TAB);
        DriverUtils.scrollElementToCenter(booksTab, driver);
        booksTab.click();
    }

    public void openNoteBooksSection() throws InterruptedException{
        WebElement notebookTab = driver.findElement(NOTEBOOKS_TAB);
        DriverUtils.scrollElementToCenter(notebookTab, driver);
        notebookTab.click();
    }
    public void validateNumberOfProductFeatureOnClassKitTab(){
        List<WebElement> productCards = driver.findElements(CLASS_KIT_PRODUCT_CARD);
        Assert.assertEquals(productCards.size(), NUMBER_OF_PRODUCTS_FEATURED_ON_CLASS_KIT_TAB, WRONG_NUMBER_OF_PRODUCTS_FEATURED_ON_CLASS_KIT_TAB);
    }

    public void validateNumberOfProductFeatureOnUniformTab(){
        List<WebElement> productCards = driver.findElements(UNIFORMS_PRODUCT_CARD);
        Assert.assertEquals(productCards.size(), NUMBER_OF_PRODUCTS_FEATURED_ON_UNIFORMS_TAB, WRONG_NUMBER_OF_PRODUCTS_FEATURED_ON_UNIFORMS_TAB);
    }

    public void openRandomProductFromClassKitTab() throws  InterruptedException{
        List<WebElement> productCards = driver.findElements(CLASS_KIT_PRODUCT_VIEW_DETAILS_BUTTON);
        int randomNumber = ThreadLocalRandom.current().nextInt(1, NUMBER_OF_PRODUCTS_FEATURED_ON_CLASS_KIT_TAB+1);
        WebElement productCard = productCards.get(randomNumber);
        DriverUtils.scrollElementToCenter(productCard, driver);
        actions.moveToElement(productCard);
        actions.perform();
        productCard.click();
    }


}
