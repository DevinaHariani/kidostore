package validations.home.exclusiveProducts.constants;

import org.openqa.selenium.By;

public interface ExclusiveProductsConstants {

    By CLASS_KIT_TAB = By.xpath("//a[@id='class-kit-tab']");
    By UNIFORMS_TAB = By.xpath("//a[@id='uniforms-tab']");
    By BOOKS_TAB = By.xpath("//a[@id='books-tab']");
    By NOTEBOOKS_TAB = By.xpath("//a[@id='notebooks-tab']");
    By CLASS_KIT_PRODUCT_CARD = By.xpath("//div[@id='class-kit']//div[@class='product product-wrapper']");
    By UNIFORMS_PRODUCT_CARD = By.xpath("//div[@id='uniforms']//div[@class='product product-wrapper']");


    By CLASS_KIT_PRODUCT_VIEW_DETAILS_BUTTON = By.xpath("//div[@id='class-kit']//div[@class='product product-wrapper']//div[@class='product_action_box']/ul/li/a[normalize-space()='View Details']");

    int NUMBER_OF_PRODUCTS_FEATURED_ON_CLASS_KIT_TAB = 8;
    int NUMBER_OF_PRODUCTS_FEATURED_ON_UNIFORMS_TAB = 8;
    String WRONG_NUMBER_OF_PRODUCTS_FEATURED_ON_CLASS_KIT_TAB = "Wrong number of products featured on class kit tab";
    String WRONG_NUMBER_OF_PRODUCTS_FEATURED_ON_UNIFORMS_TAB = "Wrong number of products featured on uniforms tab";


}
