package validations.navigation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import validations.navigation.constants.NavigationConstants;

import javax.swing.*;
import java.time.Duration;

public class NavigationValidation implements NavigationConstants {

    WebDriver driver;
    Actions actions;
    WebDriverWait wait;

    public NavigationValidation(WebDriver driver) {
        this.driver = driver;
        actions = new Actions(driver);
        wait = new WebDriverWait(driver, Duration.ofSeconds(20));
    }
    public void goToCart(){
        WebElement cartIcon = driver.findElement(CART_ICON);
        actions.moveToElement(cartIcon);
        actions.perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(VIEW_CART_BUTTON)).click();

    }
    public void goToHome(){
        driver.findElement(HOME_LINK).click();
    }

    public int getCartItemCount(){
        return Integer.parseInt(driver.findElement(CART_ITEM_COUNT_BAGDE).getText().trim());
    }
    public void clickOnSearchIcon(){
        driver.findElement(SEARCH_ICON).click();
    }
}
