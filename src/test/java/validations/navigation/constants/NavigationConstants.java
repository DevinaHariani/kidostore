package validations.navigation.constants;

import org.openqa.selenium.By;

public interface NavigationConstants {
    By CART_ICON = By.xpath("//li[@class='dropdown cart_dropdown'][1]");
    By VIEW_CART_BUTTON = By.xpath("//a[contains(@class, 'view-cart')]");
    By HOME_LINK = By.xpath("//li[@class='dropdown']/a[normalize-space()='Home']");
    By CART_ITEM_COUNT_BAGDE = By.xpath("//span[@id='cart-count']");
    By SEARCH_ICON = By.xpath("//a[@class='nav-link search_trigger']");
}
