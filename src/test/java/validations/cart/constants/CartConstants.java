package validations.cart.constants;

import org.openqa.selenium.By;

public interface CartConstants {
    String PRODUCT_NAME = "//table/tbody[@id='cart-table-body']/tr/td/a[normalize-space()='productNameToBeReplace']";
    String PRODUCT_PRICE ="//table/tbody[@id='cart-table-body']/tr/td/a[normalize-space()='productNameToBeReplace']/parent::td/parent::tr/td[@class='product-price']/span";
    String PRODUCT_QUANTITY = "//table/tbody[@id='cart-table-body']/tr/td/a[normalize-space()='productNameToBeReplace']/parent::td/parent::tr/td[@class='product-quantity']//input";
    String PRODUCT_TOTAL = "//table/tbody[@id='cart-table-body']/tr/td/a[normalize-space()='productNameToBeReplace']/parent::td/parent::tr/td[@class='product-subtotal']";
    By REMOVE_ITEM_BUTTON = By.xpath("//table/tbody[@id='cart-table-body']/tr/td/a[@class='item_remove']");

    By PRODUCT_SUB_TOTALS = By.xpath("//table/tbody[@id='cart-table-body']/tr/td[@class='product-subtotal']");
    By CART_TOTAL_FINAL_AMOUNT = By.xpath("//span[@id='total-amount']");
    By PRODUCT_NAMES = By.xpath("//table/tbody[@id='cart-table-body']/tr/td[@class='product-name']");
    By PRODUCT_QUANTITIES = By.xpath("//table/tbody[@id='cart-table-body']/tr/td[@class='product-quantity']/div/input");
    By PRODUCT_TOTALS = By.xpath("//table/tbody[@id='cart-table-body']/tr/td[@class='product-subtotal']");

    By CHECKOUT_BUTTON = By.xpath("//a[normalize-space()='Proceed To CheckOut']");

}
