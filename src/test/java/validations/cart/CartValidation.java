package validations.cart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import utils.DriverUtils;
import validations.cart.constants.CartConstants;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CartValidation implements CartConstants {
    WebDriver driver;
    WebDriverWait wait;

    HashMap<String, Object> productsForCheckout;

    public CartValidation(WebDriver driver) {

        this.driver = driver;
        wait=new WebDriverWait(driver, Duration.ofSeconds(20));
    }

    public void validateProductName(String actualProductName) {
        System.out.println("Inside product name");
        String formattedXpath = PRODUCT_NAME.replace("productNameToBeReplace", actualProductName);
        boolean isAddedProductVisible = driver.findElement(By.xpath(formattedXpath)).isDisplayed();
        Assert.assertTrue(isAddedProductVisible, "added product is not visible in cart");
    }

    public void validateProductPrice(String actualProductName, float actualProductPrice) {
        System.out.println("Inside product price");
        String formattedXpath = PRODUCT_PRICE.replace("productNameToBeReplace", actualProductName);
        float extractedProductPrice = Float.parseFloat(driver.findElement(By.xpath(formattedXpath)).getText().trim());
        Assert.assertEquals(actualProductPrice, extractedProductPrice, "Wrong Product Price shown on cart page");
    }

    public void validateProductQuantity(String actualProductName, int actualQuantity) {
        System.out.println("Inside product quantity");
        String formattedXpath = PRODUCT_QUANTITY.replace("productNameToBeReplace", actualProductName);
        int extractedProductQuantity = Integer.parseInt(driver.findElement(By.xpath(formattedXpath)).getAttribute("value"));
        Assert.assertEquals(actualQuantity, extractedProductQuantity, "Wrong quantity shown on cart page");
    }

    public void validateTotal(String actualProductName, int quantity, float pricePerItem){
        System.out.println("Inside product total");
        String formattedXpath = PRODUCT_TOTAL.replace("productNameToBeReplace", actualProductName);
        float extractedTotal = Float.parseFloat(driver.findElement(By.xpath(formattedXpath)).getText().trim());
        float calculatedTotal = pricePerItem*quantity;
        Assert.assertEquals(calculatedTotal, extractedTotal, "Wrong total shown on cart page");
    }

    public void emptyCart() throws InterruptedException{
        List<WebElement> removeItemButtons = driver.findElements(REMOVE_ITEM_BUTTON);
        for(WebElement removeItemButton: removeItemButtons){
            removeItemButton.click();
            Thread.sleep(1000);
        }
    }
    public void validateCartTotal(){

        List<WebElement> productSubTotals = driver.findElements(PRODUCT_SUB_TOTALS);
        float calculatedSubTotals = 0;
        for(WebElement productSubTotal: productSubTotals){
            calculatedSubTotals += Float.parseFloat(productSubTotal.getText().trim());
        }
        float extractedSubTotal = Float.parseFloat(driver.findElement(CART_TOTAL_FINAL_AMOUNT).getText().trim());
        Assert.assertEquals(calculatedSubTotals, extractedSubTotal, "Wrong quantity shown on cart page");
    }

    public void clickOnCheckOutButton(WebDriver driver) throws InterruptedException {

        WebElement checkout = driver.findElement(CHECKOUT_BUTTON);
        DriverUtils.scrollElementToCenter(checkout, driver);
       checkout.click();
    }

    public void storeProductsForCheckOut(){
        productsForCheckout = new HashMap<>();
        List<String> productNames = new ArrayList<>();
        List<Float> productTotals = new ArrayList<>();
        List<Integer> productQuantities = new ArrayList<>();

        List<WebElement> productNameElements = driver.findElements(PRODUCT_NAMES);
        for (WebElement productNamesElement: productNameElements ){
            String name = productNamesElement.getText().trim().toLowerCase();
            productNames.add(name);
        }

        List<WebElement> productQuantityElements = driver.findElements(PRODUCT_QUANTITIES);
        for (WebElement productQuantityElement: productQuantityElements ){
            int quantity = Integer.parseInt(productQuantityElement.getAttribute("value"));
            productQuantities.add(quantity);
        }

        List<WebElement> productTotalElements=driver.findElements(PRODUCT_TOTALS);
        for(WebElement productTotalElement : productTotalElements){
            float total=Float.parseFloat(productTotalElement.getText().trim());
            productTotals.add(total);
        }
    }
}
