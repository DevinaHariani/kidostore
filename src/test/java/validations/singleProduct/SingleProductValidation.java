package validations.singleProduct;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import validations.singleProduct.constants.SingleProductConstants;
import org.testng.Assert;

import java.time.Duration;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

public class SingleProductValidation implements SingleProductConstants {
    WebDriver driver;

    HashMap<String, Object> productData;

    WebDriverWait wait;

    public SingleProductValidation(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        productData = new HashMap<>();
    }

    public void increaseQuantity() {
        int quantity = ThreadLocalRandom.current().nextInt(1, 5);
        driver.findElement(QUANTITY_INPUT).clear();
        driver.findElement(QUANTITY_INPUT).sendKeys(Integer.toString(quantity));
    }

    public void clickOnAddToCartButton() {
        driver.findElement(ADD_TO_CART_BUTTON).click();
    }


    public void validateSuccessfullyAddedToCartMessage() {
        boolean isMessageDisplayed = wait.until(ExpectedConditions.visibilityOfElementLocated(SUCCESSFULLY_ADDED_TO_CART_MESSAGE)).isDisplayed();
        Assert.assertTrue(isMessageDisplayed, "there is some problem in add to cart feature!!!");
    }

    public void storeProductData() {
        productData.put("title", getName());
        productData.put("price", getPrice());
        productData.put("variant", getVariant());
        productData.put("quantity", getQuantity());
    }

    public String getName() {
        return driver.findElement(TITLE).getText();
    }

    public Float getPrice() {
        return Float.parseFloat(driver.findElement(PRICE).getText().trim());
    }

    public String getVariant() {
        return driver.findElement(VARIANT).getText().trim();
    }

    public int getQuantity() {
        return Integer.parseInt(driver.findElement(QUANTITY_INPUT).getAttribute("value"));
    }
    public HashMap<String, Object> getProductData(){
        return productData;
    }
}
