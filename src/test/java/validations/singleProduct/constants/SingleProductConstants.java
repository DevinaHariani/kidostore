package validations.singleProduct.constants;

import org.openqa.selenium.By;

public interface SingleProductConstants {

    By TITLE = By.xpath("//h4[@class='product_title']");
    By PRICE = By.xpath("//div[@class='product_price']/span[@class='price']");
    By VARIANT = By.xpath("//div[@data-type='product-variation']//span[contains(@class, 'active')]");
    By QUANTITY_INPUT = By.name("quantity");
    By ADD_TO_CART_BUTTON = By.xpath("//button[normalize-space()='Add to cart']");
    By SUCCESSFULLY_ADDED_TO_CART_MESSAGE = By.xpath(
            "//div[contains(text(),'Successfully Added to cart!')]");

}
