package validations.location.constants;

public interface LocationConstants {
    String BASE_URL = "https://kidostore.in";
    String LOGIN_ROUTE = "/login";
}
