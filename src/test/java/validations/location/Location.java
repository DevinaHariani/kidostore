package validations.location;

import org.openqa.selenium.WebDriver;
import validations.location.constants.LocationConstants;

public class Location implements LocationConstants {
    WebDriver driver;

    public Location(WebDriver driver) {
        this.driver = driver;
    }

    public void visitLoginPage(){
        driver.get(BASE_URL+LOGIN_ROUTE);
    }
}
