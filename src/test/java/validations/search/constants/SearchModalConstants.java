package validations.search.constants;

import org.openqa.selenium.By;

public interface SearchModalConstants {

    By SEARCH_MODAL = By.xpath("//div[@class='search_wrap open']");
    By SEARCH_INPUT = By.xpath("//input[@id='search-input']");

}
