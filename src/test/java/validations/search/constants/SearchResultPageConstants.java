package validations.search.constants;

import org.openqa.selenium.By;

public interface SearchResultPageConstants {

    By PRODUCT_TITLE = By.xpath("//div[@class='product product-wrapper']/div[@class='product_info']/h6/a");
}
