package validations.search;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import validations.search.constants.SearchResultPageConstants;

import java.util.List;

public class SearchResultPageValidation implements SearchResultPageConstants {

    WebDriver driver;
    public SearchResultPageValidation(WebDriver driver) {
        this.driver = driver;
    }
    public void validateProductSearchWithExactName(String actualSearchProductName) {
        String extractedSearchResultProductName = driver.findElement(PRODUCT_TITLE).getText().trim().toLowerCase();
        Assert.assertEquals(actualSearchProductName.toLowerCase(), extractedSearchResultProductName, "Wrong product shown when searched for exact product search");

    }
    public void validateProductSearchedWithKeywords(String actualSearchKeywords) {
        List<WebElement> productNames = driver.findElements(PRODUCT_TITLE);
        for (WebElement productName : productNames) {
            boolean isKeywordPresent = productName.getText().contains(actualSearchKeywords);
            Assert.assertTrue(isKeywordPresent, "fetched product that doesnt have searched keyword");
        }
    }
}
