package validations.login.constants;

import org.openqa.selenium.By;

public interface LoginConstants {
    By USERNAME_INPUT = By.xpath("//input[@id='email']");
    By PASSWORD_INPUT = By.xpath("//input[@id='password']");
    By LOGIN_BUTTON = By.xpath("//button[@id='btn-submit']");

    By ERROR_MESSAGE = By.className("invalid-feedback");
}
