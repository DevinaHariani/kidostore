package validations.login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import validations.loader.constants.LoaderConstants;
import validations.login.constants.LoginConstants;

import java.time.Duration;

public class LoginValidation implements LoginConstants {

    WebDriver driver;
    WebDriverWait wait;


    public LoginValidation(WebDriver driver){
        this.driver=driver;
        wait=new WebDriverWait(driver, Duration.ofSeconds(20));
    }

    public void enterUsername(String username){
        driver.findElement(USERNAME_INPUT).sendKeys(username);
    }
    public void enterPassword(String password){
        driver.findElement(PASSWORD_INPUT).sendKeys(password);
    }
    public void clickOnLogInButton(){
        driver.findElement(LOGIN_BUTTON).click();
    }

    public boolean isErrorMessageDisplayed() {
        WebElement errorMessage = driver.findElement(ERROR_MESSAGE);
        return errorMessage.isDisplayed();
    }
    public String getErrorMessageText() {
        WebElement errorMessage = driver.findElement(ERROR_MESSAGE);
        return errorMessage.getText();
    }


}
