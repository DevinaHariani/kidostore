package factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverFactory {
    public static ThreadLocal<WebDriver> tl = new ThreadLocal<>();

    public void initDriver(){
        tl.set(new ChromeDriver());
        getDriver().manage().window().maximize();
    }

    public static WebDriver getDriver(){
        return tl.get();
    }
}
